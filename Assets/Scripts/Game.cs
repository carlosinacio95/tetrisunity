﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class Game : MonoBehaviour
{
    //comprimento da grelha
    public static int gridWidth = 10;
    //Altura da grelha
    public static int gridHeight = 20;
    //a grelha
    public static Transform[,] grid = new Transform[gridWidth, gridHeight];

    public static bool startingAtLevelZero;
    public static int startingLevel;

    public int scoreOneLine = 40;
    public int scoreTwoLine = 100;
    public int scoreThreeLine = 300;
    public int scoreFourLine = 1200;


    public int currentLevel = 0;
    private int numLinesCleared = 0;


    public Text hud_score;
    public Text hud_levels;
    public Text hud_lines;

    private int numberOfRowsThisTurn = 0;
    public static int currentScore = 0;

    public AudioClip clearedLineSound;
    private AudioSource audioSource;

    private GameObject previewTetris;
    private GameObject nextTetris;
    private bool gameStarted = false;

    public static float fallSpeed = 1.0f;

    private Vector2 previewTetrisPosition = new Vector2(-6.5f, 16);

    // Start is called before the first frame update
    void Start()
    {
        currentScore = 0;

        hud_score.text = "0";
        hud_levels.text = "0";
        currentLevel = startingLevel;
       

        hud_lines.text = "0";

        SpawnNextTetris();
        audioSource = GetComponent<AudioSource>();
    }
    
    void Update()
    {
        UpdateScore();
        UpdateUI();
        UpdateLevel();
        UpdateSpeed();
    }

    void UpdateLevel()
    {
        if ((startingAtLevelZero == true) ||(startingAtLevelZero == false && numLinesCleared / 10 > startingLevel))
        {
            currentLevel = numLinesCleared / 10;
        }
    }

    void UpdateSpeed()
    {
        fallSpeed = 1.0f - ((float)currentLevel * 0.1f);
    }

    public void UpdateUI()
     {
        hud_score.text = currentScore.ToString();
        hud_levels.text = currentLevel.ToString();
        hud_lines.text = numLinesCleared.ToString();
      }

    public void UpdateScore()
    {
    if(numberOfRowsThisTurn > 0)
    {
    if(numberOfRowsThisTurn == 1)
            {
                ClearedOneLine();
            }
            else if(numberOfRowsThisTurn == 2)
            {
                ClearedTwoLines();
            }
            else if(numberOfRowsThisTurn == 3)
            {
                ClearedThreeLines();
            }
            else if(numberOfRowsThisTurn == 4)
            {
                ClearedFourLines();
            }
            numberOfRowsThisTurn = 0;
            PlayLineClearedSound();
        }
    }

    public void ClearedOneLine()
       {
        currentScore += scoreOneLine + (currentLevel *10);
        numLinesCleared++;
       }

    public void ClearedTwoLines()
       {
        currentScore += scoreTwoLine + (currentLevel * 15);
        numLinesCleared += 2;
       }

    public void ClearedThreeLines()
       {
        currentScore += scoreThreeLine + (currentLevel * 20);
        numLinesCleared += 3;
       }

    public void ClearedFourLines()
       {
        currentScore += scoreFourLine + (currentLevel * 25);
        numLinesCleared += 4;
       }

    public void PlayLineClearedSound()
    {
        audioSource.PlayOneShot(clearedLineSound);
    }

    // verificar se está acima de grelha
    public bool CheckIsAboveGrid(Tetris tetris)
    {
        for(int x = 0; x < gridWidth; ++x)
        {
            foreach (Transform mino in tetris.transform)
            {
                Vector2 pos = Round(mino.position);
                if(pos.y > gridHeight - 1)
                {
                    return true;
                }
            }
        }
        return false;
    }

    // verificar o interior da grelha
    public bool CheckIsInsideGrid(Vector2 pos)
    {
        return ((int)pos.x >= 0 && (int)pos.x < gridWidth && (int)pos.y >= 0);
    }

    //Determina se a Fila está cheia no y especifico
    public bool IsFullRowAt(int y)
    {
        for(int x = 0; x < gridWidth; ++x)
        {
            if(grid[x,y] == null)
            {
                return false;
            }

        }
        //como se encontrou uma linha completa, incrementamos a variavel da linha cheia.
        numberOfRowsThisTurn++;
        return true;
    }

    //Elimina a Linha no y 
    public void DeletePecaAt(int y)
    {
        for(int x = 0; x < gridWidth; ++x)
        {
            Destroy(grid[x, y].gameObject);
            grid[x, y] = null;
        }
    }

    //mover a linha para baixo
    public void MoveRowDown(int y)
    {
        for(int x = 0; x < gridWidth; ++x)
        {
            if(grid[x,y] != null)
            {
                grid[x, y - 1] = grid[x, y];

                grid[x, y] = null;

                grid[x, y - 1].position += new Vector3(0, -1, 0);
            }
        }
    }

    //mover as linhas todas para baixo
    public void MoveAllRowsDown(int y)
    {
        for(int i = y; i< gridHeight; ++i)
        {
            MoveRowDown(i);
        }
    }

    // remover a linha
    public void DeleteRow()
    {
        for(int y = 0; y < gridHeight; ++y)
        {
            if (IsFullRowAt(y))
            {
                DeletePecaAt(y);
                MoveAllRowsDown(y + 1);
                --y;
            }
        }
    }

    // atualizar a grelha
    public void UpdateGrid(Tetris tetris)
    {
        for(int y = 0; y< gridHeight; ++y)
        {
            for(int x = 0; x< gridWidth; ++x)
            {
                if(grid[x,y] != null)
                {
                    if(grid[x,y].parent == tetris.transform)
                    {
                        grid[x, y] = null;
                    }
                }
            }
        }
        foreach(Transform mino in tetris.transform)
        {
            Vector2 pos = Round(mino.position);
            if(pos.y < gridHeight)
            {
                grid[(int)pos.x, (int)pos.y] = mino;
            }
        }
    }

    //usar transform na posiçao da grelha
    public Transform GetTransformAtGridPosition(Vector2 pos)
    {
        if(pos.y > gridHeight - 1)
        {
            return null;
        }
        else
        {
            return grid[(int)pos.x, (int)pos.y];
        }
    }

    // criar proximo Tetris
    public void SpawnNextTetris()
    {

        if (!gameStarted)
        {
            gameStarted = true;

            nextTetris = (GameObject)Instantiate(Resources.Load(GetRandomTetris(), typeof(GameObject)), new Vector2(5.0f, 20.0f), Quaternion.identity);
            previewTetris = (GameObject)Instantiate(Resources.Load(GetRandomTetris(), typeof(GameObject)), previewTetrisPosition, Quaternion.identity);
            previewTetris.GetComponent<Tetris>().enabled = false;
        }
        else
        {
            previewTetris.transform.localPosition = new Vector2(5.0f, 20.0f);
            nextTetris = previewTetris;
            nextTetris.GetComponent<Tetris>().enabled = true;

            previewTetris = (GameObject)Instantiate(Resources.Load(GetRandomTetris(), typeof(GameObject)), previewTetrisPosition, Quaternion.identity);
            previewTetris.GetComponent<Tetris>().enabled = false;
        }
        
    }

    // Round a pos especifico
    public Vector2 Round (Vector2 pos)
    {
        return new Vector2(Mathf.Round(pos.x), Mathf.Round(pos.y));
    }

    // buscar o tetris aleatorio
    string GetRandomTetris()
    {
        int randomTetris = Random.Range(1, 8);
        string randomTetrisName = "Prefabs/Tetris_T";

        switch (randomTetris)
        {
            case 1:
                randomTetrisName = "Prefabs/Tetris_T";
                break;
            case 2:
                randomTetrisName = "Prefabs/Tetris_Long";
                break;
            case 3:
                randomTetrisName = "Prefabs/Tetris_Square";
                break;
            case 4:
                randomTetrisName = "Prefabs/Tetris_J";
                break;
            case 5:
                randomTetrisName = "Prefabs/Tetris_L";
                break;
            case 6:
                randomTetrisName = "Prefabs/Tetris_S";
                break;
            case 7:
                randomTetrisName = "Prefabs/Tetris_Z";
                break;
        }
        return randomTetrisName;
    }

    // carregar o GameOver Scene
    public void GameOver()
    {
        Application.LoadLevel("GameOver");
    }

}
